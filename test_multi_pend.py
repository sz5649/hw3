# Double pendulum formula translated from the C code at
# http://www.physics.usyd.edu.au/~wheat/dpend_html/solve_dpend.c
import matplotlib
matplotlib.use('TKAgg')
from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
from random import randint


class Pendulum_Simulation:
    def __init__(self):
        self._nlinks = 20 # default # of pendulums
        self._G =  9.8 # acceleration due to gravity, in m/s^2
        self._L = np.ones((self._nlinks, 1))  # use an array to store the Lengths
        self._M = np.ones((self._nlinks, 1)) # use an array to store the mass
        self._states = np.zeros((self._nlinks * 2)) # state has 2 * n elements

        self._generate_figure()
        self._generate_plot_objects()

    def _generate_figure(self):
        self._figure = plt.figure()
        self._ax = self._figure.add_subplot(
            111, autoscale_on=False, xlim=(-self._nlinks, self._nlinks), ylim=(-self._nlinks, self._nlinks))
        self._ax.grid()

    def _generate_plot_objects(self):
        self._animated_line, = self._ax.plot([], [], 'o-', lw=2)
        self._time_template = 'time = %.1fs'
        self._time_text = self._ax.text(
            0.05, 0.9, '', transform=self._ax.transAxes)

    def get_figure(self):
        return self._figure

    def _derivs(self, state, t):

        dydx = np.zeros_like(state)
        # dydx = self._states
        # dydx[0] = state[1]

        # del_ = state[2]-state[0]
        # den1 = ((self._M1+self._M2)*self._L1 
        #         - self._M2*self._L1*cos(del_)*cos(del_))
        # dydx[1] = (self._M2*self._L1*state[1]*state[1]*sin(del_)*cos(del_)
        #            + self._M2*self._G*sin(state[2])*cos(del_) + 
        #            self._M2*self._L2*state[3]*state[3]*sin(del_)
        #            - (self._M1+self._M2)*self._G*sin(state[0]))/den1

        # dydx[2] = state[3]

        # den2 = (self._L2/self._L1)*den1
        # dydx[3] = (-self._M2*self._L2*state[3]*state[3]*sin(del_)*cos(del_)
        #            + (self._M1+self._M2)*self._G*sin(state[0])*cos(del_)
        #            - (self._M1+self._M2)*self._L1*state[1]*state[1]*sin(del_)
        #            - (self._M1+self._M2)*self._G*sin(state[2]))/den2

        # calculate velocities
        for i in range(0, self._nlinks):
            dydx[i * 2] = state[i * 2 + 1]

        # calculate accels
        n = self._nlinks
        for i in range(0, self._nlinks - 1):
            # dydx[i * 2 + 1] = 0.05
            dydx[i * 2 + 1] = ((n-i+2)*state[(i-1)*2] - 2*(n-i+1)*state[(i)*2] + (n+2-i)*state[(i+1)*2]) * self._G / self._L[i]

        dydx[-1] = 2 * (state[-2-2] - state[-2]) * self._G / self._L[-1]
        # dydx[-1] = 0.05

        # print dydx
        return dydx

    def simulate(self):

        # create a time array from 0..100 sampled at 0.1 second steps
        self._dt = 0.05
        t = np.arange(0.0, 20, self._dt)

        # assign initial positions to be 60 +- 10 degrees
        for i in range(0, self._nlinks):
            self._states[2 * i] = (randint(0, 20)) * np.pi / 180


        # integrate your ODE using scipy.integrate.
        # print self._states
        self._y = integrate.odeint(self._derivs, self._states, t)
        # print self._y.shape
        # print len(t)

        self._xpos = np.zeros((len(t), self._nlinks))
        self._ypos = np.zeros((len(t), self._nlinks))
        # print self._xpos.shape
        self._xpos[:, 0] = self._L[0] * sin(self._y[:, 0])
        self._ypos[:, 0] = -self._L[0] * cos(self._y[:, 0])


        for i in range(1, self._nlinks):
            self._xpos[:, i] = self._L[i] * sin(self._y[:, 2 * i]) + self._xpos[:, i -1]
            self._ypos[:, i] = -self._L[i] * cos(self._y[:, 2 * i]) + self._ypos[:, i -1]

        # self._x1 = self._L1*sin(self._y[:,0])
        # self._y1 = -self._L1*cos(self._y[:,0])

        # self._x2 = self._L2*sin(self._y[:,2]) + self._x1
        # self._y2 = -self._L2*cos(self._y[:,2]) + self._y1

        
    def _init_animation_objects(self):
        self._animated_line.set_data([], [])
        self._time_text.set_text('')
        return self._animated_line, self._time_text

    def _animate(self,i):
        # thisx = [0, self._x1[i], self._x2[i]]
        # thisy = [0, self._y1[i], self._y2[i]]
        thisx = np.zeros((self._nlinks + 1))
        thisy = np.zeros((self._nlinks + 1))
        # why is self._xpos[-1] is zero?????
        for j in range(0, self._nlinks):
            thisx[j + 1] = self._xpos[i, j]
            thisy[j + 1] = self._ypos[i, j]

        # print self._xpos[i, self._nlinks - 1]


        self._animated_line.set_data(thisx, thisy)
        self._time_text.set_text(self._time_template%(i*self._dt))
        return self._animated_line, self._time_text

    def get_y(self):
        return self._y
    def get_y1(self):
        return self._y1
    def get_y2(self):
        return self._y2
    def get_x1(self):
        return self._x1
    def get_x2(self):
        return self._x2

    def get_G(self):
        return self._G
    def get_L1(self):
        return self._L1
    def get_L2(self):
        return self._L2
    def get_M1(self):
        return self._M1
    def get_M2(self):
        return self._M2
    def set_G(self,new_G):
        self._G=new_G
    def set_L1(self,new_L1):
        self._L1=new_L1
    def set_L2(self,new_L2):
        self._L2=new_L2
    def set_M1(self,new_M1):
        self._M1=new_M1
    def set_M2(self,new_M2):
        self._M2=new_M2

    def begin_animation(self):
        self._ani = animation.FuncAnimation(
            self._figure, self._animate, np.arange(1, len(self._y)),
            interval=25, blit=True, init_func=self._init_animation_objects)
        #ani.save('double_pendulum.mp4', fps=15, clear_temp=True)
            

def main():
    sim=Pendulum_Simulation()
    # sim.simulate()
    # plt.show() # we could plot here if we want.
    sim.set_G(-sim.get_G())
    sim.simulate()
    sim.begin_animation()
    plt.show()

if __name__=="__main__":
    main()