
import Tkinter as tk

import numpy as np
import matplotlib
matplotlib.use('TkAgg')

from multi_pendulum_system import *
from numpy import arange, sin, pi, cos
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
from multi_pendulum_system import *



class Application(tk.Frame):

    def __init__(self, master=None):

        self.pend = Pendulum_Simulation()
        tk.Frame.__init__(self, master)
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.createWidgets()
        self.pack()
        
        #Parameters for the simulation
        self.g = -9.81 
        self.links = 20
        self.ic = np.zeros((self.links,0))
        self.m = 0.5
        self.l = 1
        
        

    def say_hi(self):
        print "hi there, everyone!"

    def show_values(self):
        print (self.w1.get(), self.w2.get())


    def createWidgets(self):
        # create a left frame
        self.left_frame = tk.Frame(master=self)
        self.left_frame.grid(column=0, row=0)
        # self.left_frame.columnconfigure(0, minsize = 100)
        self.left_frame.columnconfigure(5, minsize = 10)
        self.left_frame.rowconfigure(5, minsize = 30)
        # self.left_frame.rowconfigure(25, minsize = 10)
        self.left_frame.rowconfigure(35, minsize = 10)
        self.left_frame.rowconfigure(65, minsize = 30)

        # create labels
        self.label1 = tk.Label(self.left_frame, text="Num of links(1-20):")
        self.label1.grid(column=0, row = 40)
        self.label2 = tk.Label(self.left_frame, text="Mass:")
        self.label2.grid(column=0, row = 10)
        self.label2 = tk.Label(self.left_frame, text="Len of links:")
        self.label2.grid(column=10, row = 10)
        self.label3 = tk.Label(self.left_frame, text="Status of link #:")
        self.label3.grid(column=0, row = 80)
        self.label4 = tk.Label(self.left_frame, text="Update")
        self.label4.grid(column=0, row = 60)
        self.label5 = tk.Label(self.left_frame, text="Theta & Omega")
        self.label5.grid(column=0, row = 70)

        #Create sliders    
        # used for adjusting the mass   
        self.w1 = tk.Scale(self.left_frame, from_=0.00, to=1.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.w1.grid(column=0, row = 20)
        self.w1.set(0.5)
        # used for adjusting the length
        self.w2 = tk.Scale(self.left_frame, from_=0.00, to=2.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.w2.grid(column=10, row = 20)
        self.w2.set(1.0)
        
        #Create buttons                
        # the quit function is inherited from the Frame class.
        self.quit = tk.Button(self.left_frame, text="Quit", command=self.quit).grid(column=10,row=0, pady=10)
        self.run_sim = tk.Button(self.left_frame, text="Run", command=self.run_simulation).grid(column=0,row=0, pady=10)
        self.plot = tk.Button(self.left_frame, text="Plot", command=self.plot).grid(column=10,row=70, pady=10, columnspan = 15)
        self.show = tk.Button(self.left_frame, text='Show Sliders', command=self.show_values).grid(column=10, row=30, pady=10, columnspan = 15)   
        self.rerun = tk.Button(self.left_frame, text="Rerun", command=self.rerun).grid(column=10, row=60, pady=10, columnspan = 15)     


        # Create a drop down list
        self.var = tk.StringVar(self.left_frame)
        self.var.set("0")
        # default to 2 links
        self.option = tk.OptionMenu(self.left_frame, self.var, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9","10", "11", "12", "13", "14", "15", "16", "17", "18", "19")
        self.option.grid(column = 10, row = 80, pady = 10)
        # add an event listener
        self.var.trace('w', self.on_option_changed)
        

        #Create entry         
        self.entrythingy = tk.Entry(self.left_frame)
        label_grav = tk.Label(self.left_frame, text="Gravity:")
        label_grav.grid(column = 0, row = 50)
        self.entrythingy.grid(column = 10, row = 50, pady = 10)

        #Create string variable, event listner
        self.contents = tk.StringVar()
        # set it to some value
        self.contents.set("9.81")
        
        # tell the entry widget to watch this variable
        ##################### how could the program know how to change self.contents?
        self.entrythingy["textvariable"] = self.contents
        # and here we get a callback when the user hits return.
        # we will have the program print out the value of the
        # application variable when the user hits return
        self.entrythingy.bind('<Key-Return>', self.print_contents)

        #Create another entry for number of links  
        # How does this incorporate into a master frame?      
        self.num_links_entry = tk.Entry(self.left_frame)
        self.num_links_entry.grid(column = 10, row = 40, pady = 10)
        # label_nlinks = tk.Label(self.left_frame, text="num links")
        # label_nlinks.grid(column = 1, row = 9)
        self.nlinks = tk.StringVar()
        self.nlinks.set("2")
        self.num_links_entry["textvariable"] = self.nlinks
        self.num_links_entry.bind('<Key-Return>', self.print_nlinks)

        # create right frame
        self.right_frame = tk.Frame(master=self)
        self.right_frame.grid(column=2, row=0, pady=5)

        # # Create the animation window in the right frame
        self.generate_plot_frame(self.right_frame)


    # when the option menu item is changed, do something
    def on_option_changed(self, *args):
        # print self.var.get()
        self.update_plot()


    def generate_plot_frame(self, parent):
        # Create the animation window
        self.plot_frame = tk.Frame(master=parent)
        self.plot_frame.grid(column=10,row=1, pady=5)
        self.plot_frame.grid_columnconfigure(10,weight=2)
        tk.Label(master = self.plot_frame, text="plot").pack()
        # self._fig = plt.figure()
        self._fig = self.pend.get_figure()
        self.canvas = FigureCanvasTkAgg(self._fig, master = self.plot_frame)
        # self.ax = self._fig.add_subplot(111, xlim=(0, 2), ylim=(-2, 2))
        self.ax = self.pend._ax
        self._x = np.arange(0, 10, 0.01)
        self._animated_line, = self.ax.plot([],[])
        self._animated_line.set_data([], [])
        self.canvas.get_tk_widget().pack()




    # plot the desired variables 
    def plot(self):
        link_num = int(self.var.get())
        # self.ax = self.fig.add_subplot(111, xlim=(0, 2), ylim=(-2, 2))
        # self.animated_line, = self.ax.plot()
        fig = Figure(figsize=(4,4.5), dpi=100)
        amplitude = self.w1.get()
        freq = self.w2.get()
        self.xplot = fig.add_subplot(211)
        self.yplot = fig.add_subplot(212)
        # self.thplot = fig.add_subplot(413)
        # self.wplot = fig.add_subplot(414)
        t = np.arange(0.0, 20, 0.05)

        xpos, ypos, th, w = self.pend.get_sim_data(link_num)
        # if (link_num == 1):
        #     s = amplitude*sin(2*pi*t*freq)
        # else:
        #     s = amplitude*cos(2*pi*t*freq)
        self.xplot.plot(t, xpos)
        self.xplot.set_ylabel("$x$")
        self.yplot.plot(t, ypos)
        self.yplot.set_ylabel("$y$")
        # self.thplot.plot(t, th)
        # self.wplot.plot(t, w)



        # self.theta_1_ax = fig.add_subplot(411)
        # self.theta_1_line, = self.theta_1_ax.plot(self.sim.get_data()[:,4], self.sim.get_data()[:,0])
        # self.theta_1_dot, = self.theta_1_ax.plot(0,0,'ro')
        # self.theta_1_ax.set_xlabel("$t$")
        # self.theta_1_ax.set_ylabel(r"$\theta_{1}$")


        # a tk.DrawingArea
        self.canvas = FigureCanvasTkAgg(fig, master=self.right_frame)
        self.canvas.show()
        self.canvas.get_tk_widget().grid(column=20, row= 1, columnspan=12)



    def update_plot(self):
        link_num = int(self.var.get())
        amplitude = self.w1.get()
        freq = self.w2.get()
        t = arange(0.0, 20, 0.05)

        xpos, ypos, th, w = self.pend.get_sim_data(link_num)
        self.xplot.plot(t, xpos)
        self.yplot.plot(t, ypos)
        # self.thplot.plot(t, th)
        # self.wplot.plot(t, w)

        self.xplot.clear()
        self.yplot.clear()
        self.xplot.plot(t, xpos)
        self.yplot.plot(t, ypos)
        # self.thplot.plot(t, th)
        # self.wplot.plot(t, w)
        self.canvas.draw()


        
    # print the gravity value of the current simulation
    def print_contents(self, event):
        print "The value of gravity is now ---->", self.contents.get()

    # print the number of links of the current simulation
    def print_nlinks(self, event):
        print "The number of links is now ----->", self.nlinks.get()


    #set number of links        
    def set_number_links(self):
        linknum = int(self.nlinks.get())
        # if the number of links input is ilegal, set it to default
        self.links = linknum if (linknum > 0 and linknum < 50) else 2
    
    #set the initial conditions
    def set_ic(self):
        self.ic = np.zeros((self.links,1))
    
    #Set the gravity
    def set_gravity(self):
        # this function reads the number entered in the entrythingy, and set the gravity
        self.g = float(self.contents.get())
        
    #We can use the same function to change all the parameters (dictionary, id=>variable)
    def change_parameter(self):
        self.set_number_links()
        self.set_ic()
        self.set_gravity()
        # self.print_parameters()
        
        self.pend.change_params(self.links, self.g, self.l, self.m)


    # print the current parameters, for sanity check
    def print_parameters(self):
        print "the gravity is %f" % self.g
        print "the number of links is %d" % self.links
        print "the initial condition is :"
        print self.ic


    #Re-run simulation
    def rerun(self):
        self.change_parameter()
        self.pend.simulate()

    
    # Function to run the forward dynamics simulation
    # if arguments are None the function will use the ic and gravity from the class
    def run_simulation(self, ic=None, gravity=None):
        self.ax.clear()
        self.pend.simulate()
        self.pend.begin_animation()


    # animation step computation
    def animate(self, i):
        pass

    # animation execution
    def run_animation(self):
        pass



root = tk.Tk()

app = Application(master=root)
app.master.title("Homework 3")
app.mainloop()

root.destroy()

